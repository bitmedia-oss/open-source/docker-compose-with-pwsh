Install-Module Pester -Force -RequiredVersion 5.3.0
Install-Module powershell-yaml -Force -RequiredVersion 0.4.2
Install-Module Alt3.Docusaurus.Powershell -Force -RequiredVersion 1.0.17