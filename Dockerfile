FROM docker:20.10.12-dind
ARG CI_COMMIT_TAG 
ARG CI_COMMIT_REF_NAME 
ARG CI_COMMIT_REF_SLUG
ARG CI_COMMIT_SHA

COPY docker-compose /usr/local/bin/
ADD https://github.com/PowerShell/PowerShell/releases/download/v7.2.1/powershell-7.2.1-linux-alpine-x64.tar.gz /tmp/powershell.tar.gz
ADD https://github.com/docker/compose/releases/download/v2.3.2/docker-compose-linux-aarch64 /root/.docker/cli-plugins/docker-compose
COPY install.* /tmp/
RUN /tmp/install.sh
ENTRYPOINT [ "/bin/sh" ]
