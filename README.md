# docker-compose-with-pwsh

A docker image to help with GitLab CI builds, based on docker:dind

Contains:

* docker-compose v2.0.0-rc.3
* docker 20.10.8 (dind)
* PowerShell 7.1.4
  * Pester 5.3.0
  * Powershell-Yaml 0.4.2
  * Alt3.Docusaurus.Powershell 1.0.17
* Git
* Curl
  