#!/bin/sh
## This script will not work in bash, only in zsh (alpine image default).

set -o pipefail
set -o errexit

if [ -n "$CI_REGISTRY_USER" ]; then
    docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
fi

if [ -z "$CI_REGISTRY_IMAGE" ]; then
    CI_REGISTRY_IMAGE='registry.gitlab.com/bitmedia-oss/open-source/docker-compose-with-pwsh'
fi

export TAG=dev
if [ -n "$CI_COMMIT_REF_SLUG" ]; then
    TAG=wip-$CI_COMMIT_REF_SLUG
fi
if [ -n "$CI_COMMIT_TAG" ]; then
    TAG=$CI_COMMIT_TAG
fi
IMG_TAG=$CI_REGISTRY_IMAGE:$TAG
echo "Building $IMG_TAG"

docker build --build-arg CI_COMMIT_TAG=$CI_COMMIT_TAG \
    --build-arg CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME \
    --build-arg CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG \
    --build-arg CI_COMMIT_SHA=$CI_COMMIT_SHA \
	-f ./Dockerfile \
    -t $IMG_TAG .

if [ -n "$CI_REGISTRY_USER" ]; then
    echo "Pushing image"
    docker push $IMG_TAG
fi
